package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"


	"encoding/json"
)

type Movies struct {      
	MovieNumber primitive.ObjectID     `json:"_id,omitempty" bson:"_id,omitempty"`
	MovieName string    `json:"movieName"  bson:"movieName"`
	Imbd int     `json:"imbd" bson:"imbd"`
	MovieCategory string   `json:"movieCategory" bson:"movieCategory"`
}


func ConnectDB() *mongo.Collection {

	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	collection := client.Database("server").Collection("movies")

	return collection
}

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/api/movies", getMovies).Methods("GET")
	r.HandleFunc("/api/movies", postMovies).Methods("POST")
	r.HandleFunc("/api/movies/{id}", deleteMovie).Methods("DELETE")
	fmt.Println("Server Start")
	/*headersOk := handlers.AllowedHeaders([]string{"*"})
    originsOk := handlers.AllowedOrigins([]string{"*"})
    methodsOk := handlers.AllowedMethods([]string{"*"})

// start server listen
// with error handling
log.Fatal(http.ListenAndServe(":8080", handlers.CORS(originsOk, headersOk, methodsOk)(r)))*/
	log.Fatal(http.ListenAndServe(":8080", r))
	//log.Fatal(Origin = "./localhost:3000",r)
	
}
func getMovies(w http.ResponseWriter, r *http.Request) {
	collection := ConnectDB()
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	moviesList:= []Movies{}
	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Fatal(err)
		return
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

    movie:= Movies{}

		err := cur.Decode(&movie)
		if err != nil {
			log.Fatal(err)
		}
		
		moviesList = append(moviesList, movie)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(moviesList)
}

func postMovies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "*")

	var movie Movies
	_ = json.NewDecoder(r.Body).Decode(&movie)
	collection := ConnectDB()
	result, err := collection.InsertOne(context.TODO(), movie)
	if err != nil {
		log.Fatal(err)
		return
	}

	json.NewEncoder(w).Encode(result)

}

func deleteMovie(w http.ResponseWriter, r *http.Request) {
	collection := ConnectDB()
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	var params = mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil {
		log.Fatal(err)
		return
	}
	filter := bson.M{"_id": id}
	deleteResult, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
		return
	}
	json.NewEncoder(w).Encode(deleteResult)
}